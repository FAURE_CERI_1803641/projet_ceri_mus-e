package com.example.projetcerimuse.ui.home;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetcerimuse.API.DataFetcher;
import com.example.projetcerimuse.API.DatabaseHelper;
import com.example.projetcerimuse.MainActivity;
import com.example.projetcerimuse.Model.Appareil;
import com.example.projetcerimuse.Model.AppareilList;
import com.example.projetcerimuse.R;
import com.example.projetcerimuse.ui.AppareilActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    private static final String[] items = new String[] {};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        // Partie création/formation de la base de données

        final DatabaseHelper dbHelper = new DatabaseHelper(this.getContext());
        DataFetcher df = new DataFetcher();
        df.execute(dbHelper);

        // AppareilList appareilList = new AppareilList();

        // ListView listeAppareil = (ListView) root.findViewById(R.id.listeAppareil);


/*
// Create an ArrayAdapter, that will actually make the Strings above appear in the ListView

        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, appareilList.getAppareilName());
        listeAppareil.setAdapter(adapter2);
*/
/*
// Declaration of an adapter with items with 2 elements
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_1,
                dbHelper.fetchAllAppareil(), new String[] {
                DatabaseHelper.NOM,
                DatabaseHelper.MARQUE },
                new int[] { R.id.title}, 0);
        // Associate the adapter with the list view

        listeAppareil.setAdapter(adapter);
*/

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this.getContext(),
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllAppareil(),
                new String[] { dbHelper.NOM, dbHelper.MARQUE },
                new int[] { android.R.id.text1, android.R.id.text2});
        ListView listeAppareil = root.findViewById(R.id.listeAppareil);
        listeAppareil.setAdapter(cursorAdapter);

        listeAppareil.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(parent.getContext(), AppareilActivity.class);
                final Appareil appareil =  dbHelper.cursorToAppareil((Cursor)parent.getItemAtPosition(position));
                Appareil appareilIntent = dbHelper.getAppareil((String) appareil.getId());
                Log.d("NOM APPAREIL INTENT", appareilIntent.getNom()+"");
                Log.d("MARQUE APPAREIL INTENT", appareilIntent.getMarque()+"");
                Log.d("CATEGORIES APPAREIL", appareilIntent.getCategories() +"");
                intent.putExtra("appareil", (Parcelable) appareilIntent);

                //Toast.makeText( "Vous avez sélectionné: " + appareilIntent.getNom(), Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });




        return root;
    }

    public static String[] getItems() {
        return items;
    }
}




