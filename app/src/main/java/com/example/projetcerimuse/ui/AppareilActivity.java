package com.example.projetcerimuse.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetcerimuse.Model.Appareil;
import com.example.projetcerimuse.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.InputStream;
import java.net.URL;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class AppareilActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appareil_activity);


        TextView nomAppareil = findViewById(R.id.nomAppareil);
        TextView marqueAppareil = findViewById(R.id.marqueDisplay);
        TextView categoriesDisplay = findViewById(R.id.categorieDisplay);
        TextView descriptionDisplay = findViewById(R.id.descriptionDisplay);
        TextView anneeDisplay = findViewById(R.id.anneeDisplay);
        ImageView imageDisplay = findViewById(R.id.imageView);

        Appareil display = (Appareil)getIntent().getParcelableExtra("appareil");
        Log.d("NOUVEAU", "INTENT");
        Log.d("NOM APPAREIL INTENT", display.getNom()+"");
        Log.d("MARQUE APPAREIL INTENT", display.getMarque()+"");
        Log.d("CATEGORIES APPAREIL", display.getCategories() +"");

        nomAppareil.setText(display.getNom());
        marqueAppareil.setText(display.getMarque());
        categoriesDisplay.setText(display.getCategories());
        descriptionDisplay.setText(display.getDescription());
        if (display.getAnnee() == 0){
            anneeDisplay.setText(display.getTimeFrame());
        }
        else{
            anneeDisplay.setText(display.getAnnee()+"\n"+display.getTimeFrame());
        }
        imageDisplay.setImageDrawable(ImageOperations(display.getLienIcone()));




    }

    //method taken from https://www.developpez.net/forums/d1710963/java/developpement-mobile-java/android/afficher-image-partir-d-url/
    protected Drawable ImageOperations(String url) {
        try {
            URL urle = new URL(url);
            Object content = urle.getContent();

            InputStream inputstream = (InputStream)content;
            Drawable  drawable = Drawable.createFromStream(inputstream, "src");
            return drawable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


