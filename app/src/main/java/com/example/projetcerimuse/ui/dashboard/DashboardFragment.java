package com.example.projetcerimuse.ui.dashboard;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.projetcerimuse.API.DataFetcher;
import com.example.projetcerimuse.API.DatabaseHelper;
import com.example.projetcerimuse.Model.Appareil;
import com.example.projetcerimuse.R;
import com.example.projetcerimuse.ui.AppareilActivity;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);



                    }
                });
        /*
        final DatabaseHelper dbHelper = new DatabaseHelper(this.getContext());
        DataFetcher df = new DataFetcher();
        df.execute(dbHelper);

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this.getContext(),
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCategories(),
                new String[] { dbHelper.NOM},
                new int[] { android.R.id.text1});
        ListView listeAppareil = root.findViewById(R.id.listeAppareil);
        listeAppareil.setAdapter(cursorAdapter);

        listeAppareil.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(parent.getContext(), AppareilActivity.class);
                final Appareil appareil =  dbHelper.cursorToAppareil((Cursor)parent.getItemAtPosition(position));
                Appareil appareilIntent = dbHelper.getAppareil((String) appareil.getId());
                Log.d("CATEGORIE", appareilIntent.getNom()+"");


                intent.putExtra("appareil", (Parcelable) appareilIntent);

                //Toast.makeText( "Vous avez sélectionné: " + appareilIntent.getNom(), Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });*/
        return root;
    }
}
