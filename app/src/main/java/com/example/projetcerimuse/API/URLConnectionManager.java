package com.example.projetcerimuse.API;

import android.util.JsonReader;
import android.util.Log;

import com.example.projetcerimuse.Model.Appareil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class URLConnectionManager {
    public ArrayList<Appareil> listeAppareils = new ArrayList();
    public Appareil appareil;
    //public String lienCategorie = "https://demo-lia.univ-avignon.fr/cerimuseum/categories";
    //public ArrayList<String> listeCategorie = new ArrayList();

    public ArrayList<Appareil> getListeAppareils (){
        return this.listeAppareils;
    }

    /*public ArrayList<String> getListeCategorie (){
        return this.listeCategorie;
    }*/

    public void converse(String lien) throws IOException {
        URL url = new URL(lien);
        //URLConnection conn = url.openConnection();
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStream(in);
        } finally {
            urlConnection.disconnect();
        }
        /*URL url2 = new URL(lienCategorie);
        //URLConnection conn = url.openConnection();
        HttpURLConnection urlConnection2 = (HttpURLConnection) url.openConnection();
        try {
            InputStream in2 = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStreamCategorie(in2);
        } finally {
            urlConnection.disconnect();
        }*/

        return;
    }
/*
    public void readJsonStreamCategorie(InputStream res) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(res, "UTF-8"));
        try {
            this.readCategories(reader); // parse message
        } finally {
            reader.close();
        }
        return;
    }

    public void readCategories(JsonReader reader) throws IOException{
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            String categorie = reader.nextString();
            this.listeCategorie.add(categorie);
            Log.d("CATEGORIE", categorie);
        }
        reader.endObject();
    }
*/




        public void readJsonStream(InputStream res) throws IOException {
            JsonReader reader = new JsonReader(new InputStreamReader(res, "UTF-8"));
            try {
                this.readObjects(reader); // parse message
            } finally {
                reader.close();
            }
            return;
        }

    public void readObjects(JsonReader reader) throws IOException{
        reader.beginObject();
        while (reader.hasNext()) {
            String id = reader.nextName();
            readArrayObject(reader, id);

        }
        reader.endObject();
    }

    private void readArrayObject(JsonReader reader, String id) throws IOException {
        reader.beginObject();
        int nb = 1;
        String nom = "";
        String marque = "";
        String timeFrame = "";
        int annee = 0;
        String categories = "";
        String detailsTechnique = "";
        String description = "";
        boolean fonctionnel = false;
        while (reader.hasNext()) {


            String name = reader.nextName();


                if (name.equals("name")) {
                    nom = reader.nextString();
                } else if (name.equals("brand")) {
                    marque = reader.nextString();
                } else if (name.equals("timeFrame")) {
                    reader.beginArray();
                    String timeFrameFetched = "";
                    int i = 0;
                    while (reader.hasNext()) {
                        timeFrameFetched += reader.nextInt() + "|";
                        i++;
                    }
                    timeFrame = timeFrameFetched;
                    reader.endArray();
                } else if (name.equals("year")) {
                    annee = reader.nextInt();
                } else if (name.equals("categories")) {
                    reader.beginArray();
                    String categoriesFetched = "";
                    int i = 0;
                    while (reader.hasNext()) {
                        categoriesFetched += reader.nextString() +"|";
                        i++;
                    }
                    categories = categoriesFetched;
                    reader.endArray();
                } else if (name.equals("technicalDetails")) {
                    reader.beginArray();
                    String detailsTechniqueFetched = "";
                    int i = 0;
                    while (reader.hasNext()) {
                        detailsTechniqueFetched += reader.nextString() + "|";
                        i++;
                    }
                    detailsTechnique = detailsTechniqueFetched;
                    reader.endArray();
                } else if (name.equals("description")) {
                    description = reader.nextString();
                }else if (name.equals("working")) {
                    fonctionnel = reader.nextBoolean();
                } else {
                    reader.skipValue();
                }

        }
        Appareil appareilInsert = new Appareil(id, nom, categories, description, timeFrame, annee, marque, detailsTechnique, fonctionnel, "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+id+"/thumbnail");
        listeAppareils.add(appareilInsert);
        nb ++;
        /*
        Log.d("OBJET NUMERO ",nb+"");
        Log.d("APPAREIL NOM",appareilInsert.getNom());
        Log.d("APPAREIL MARQUE",appareilInsert.getMarque());
        Log.d("APPAREIL CATEGORIES",appareilInsert.getCategories());
        Log.d("APPAREIL DESCRIPTION",appareilInsert.getDescription());
        Log.d("APPAREIL TIMEFRAM",appareilInsert.getTimeFrame());
        Log.d("APPAREIL ANNEE",appareilInsert.getAnnee() +"");
        Log.d("APPAREIL DETAILS",appareilInsert.getDetailsTechniques());
        */


        reader.endObject();

    }
}
