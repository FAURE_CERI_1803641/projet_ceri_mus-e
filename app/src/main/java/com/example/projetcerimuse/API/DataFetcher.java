package com.example.projetcerimuse.API;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.URL;

public class DataFetcher extends AsyncTask<DatabaseHelper, Integer, Long> {

    private DatabaseHelper db;

    private Context context;

    public void setDb(DatabaseHelper db){
        this.db = db;
        return;
    }

    @Override
    protected Long doInBackground(DatabaseHelper... dbhelpers) {

        URLConnectionManager APIManager = new URLConnectionManager();
        try {
            APIManager.converse("https://demo-lia.univ-avignon.fr/cerimuseum/catalog");
            dbhelpers[0].formation(APIManager.getListeAppareils());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
