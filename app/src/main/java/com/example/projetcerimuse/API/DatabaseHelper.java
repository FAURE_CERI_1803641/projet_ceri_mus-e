package com.example.projetcerimuse.API;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.projetcerimuse.Model.Appareil;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class DatabaseHelper extends SQLiteOpenHelper {


    
    private static final String DATABASE_NAME = "constants.db";
    private static final int SCHEMA = 1;
    public static final String _ID = "_id";
    public static final String NOM_TABLE = "appareil";
    public static final String ID_APPAREIL = "idappareil";
    public static final String NOM = "name";
    public static final String CATEGORIE = "categories";
    public static final String MARQUE = "marque";
    public static final String TIMEFRAME = "timeframe";
    public static final String ANNEE = "annee";
    public static final String TECHNICALDETAILS = "technicaldetails";
    public static final String DESCRIPTION = "description";
    public static final String FONCTIONNEL = "fonctionnel";
    public static final String LIENICONE = "lienIcone";

    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "+ NOM_TABLE + "("+
            _ID + " INTEGER PRIMARY KEY,"+
            ID_APPAREIL +" TEXT, " +
            NOM + " TEXT," +
            MARQUE +" TEXT," +
            TIMEFRAME +" TEXT," +
            ANNEE+" REAL," +
            CATEGORIE +" TEXT," +
            TECHNICALDETAILS+" TEXT," +
            DESCRIPTION +" TEXT," +
            FONCTIONNEL + " BOOL," +
            LIENICONE + " TEXT);"
            ;
    private static final String NOM_TABLE_CATEGORIE = "categorie";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    // Called only once, first time the DB is created
    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL("DROP TABLE IF EXISTS "+NOM_TABLE+";");
            db.execSQL("DROP TABLE IF EXISTS "+NOM_TABLE_CATEGORIE+";");
            db.execSQL(DATABASE_CREATE);
            //db.execSQL("create table IF NOT EXISTS "+NOM_TABLE_CATEGORIE+"(nom TEXT);");
        } catch (SQLException e){
            e.printStackTrace();
        }
        return;
    }

    // Called whenever newVersion != oldVersion
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Typically do ALTER TABLE statements, but...we're just in development,
        db.execSQL("drop table if exists " + NOM_TABLE); // drops the old database
        onCreate(db); // run onCreate to get new database
    }

    public void formation(ArrayList<Appareil> listeAppareil) {
        for(int i = 0; i < listeAppareil.size(); i++){
            this.addAppareil(listeAppareil.get(i));
            //this.addCategorie(listeCategorie.get(i));

        }
        return;
    }

    public boolean addCategorie(String categorie) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.rawQuery("Insert Into "+NOM_TABLE_CATEGORIE+" (nom) VALUES (\""+ categorie+"\");", null);
        db.close(); // Closing database connection

        return (true);
    }

    private ContentValues fill(Appareil appareil) {
        ContentValues values = new ContentValues();
        values.put(ID_APPAREIL, "defaut");
        values.put(NOM, appareil.getNom());
        values.put(MARQUE, appareil.getMarque());
        values.put(TIMEFRAME, appareil.getTimeFrame());
        values.put(ANNEE, appareil.getAnnee());
        values.put(CATEGORIE, appareil.getCategories());
        values.put(TECHNICALDETAILS, appareil.getDetailsTechniques());
        values.put(DESCRIPTION, appareil.getDescription());
        values.put(FONCTIONNEL, appareil.isFonctionnel());
        values.put(LIENICONE, appareil.getLienIcone());

        return values;
    }

    public boolean addAppareil(Appareil appareil) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(appareil);

        //Log.d(TAG, "adding: "+team.getName()+" with id="+team.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(NOM_TABLE, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public void insertAppareil(Appareil appareil){
        SQLiteDatabase db = this.getWritableDatabase();
        //Log.d("NOM INSERTED", appareil.getNom());
        db.execSQL("INSERT INTO "+NOM_TABLE + " ("+ID_APPAREIL+", "+NOM+", "+MARQUE+", "+TIMEFRAME+", "+ANNEE+", "+CATEGORIE+", "+TECHNICALDETAILS+", "+DESCRIPTION+","+FONCTIONNEL+","+LIENICONE+") VALUES " +
                "(\""+ appareil.getId()+"\",\""+appareil.getNom()+"\",\""+appareil.getMarque()+"\",\""+appareil.getTimeFrame()+"\","+appareil.getAnnee()+",\""+
                appareil.getCategories()+"\",\""+appareil.getDetailsTechniques()+"\",\""+appareil.getDescription()+"\",\""+appareil.isFonctionnel()+"\",\""+appareil.getLienIcone()+"\");");
        //Log.d("NOM INSERTED", appareil.getNom());
        db.close();
        return;
    }

    public Cursor fetchAllAppareil() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(NOM_TABLE, null,
                null, null, null, null, NOM +" ASC", null);
        //Cursor cursor = db.rawQuery("SELECT * FROM "+ NOM_TABLE + ";", null );
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Appareil cursorToAppareil(Cursor itemAtPosition) {
        Appareil appareil = new Appareil(itemAtPosition.getString(itemAtPosition.getColumnIndex(ID_APPAREIL)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(NOM)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(CATEGORIE)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(DESCRIPTION)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(TIMEFRAME)),
                itemAtPosition.getInt(itemAtPosition.getColumnIndex(ANNEE)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(MARQUE)),
                itemAtPosition.getString(itemAtPosition.getColumnIndex(TECHNICALDETAILS)),
                false,
                itemAtPosition.getString(itemAtPosition.getColumnIndex(LIENICONE))
                );
        Log.d("NOM APPAREIL CURSOR", itemAtPosition.getString(itemAtPosition.getColumnIndex(NOM))+"");


        return appareil;
    }

    public Appareil getAppareil(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + NOM_TABLE + " WHERE " + ID_APPAREIL + " = \""+ id +"\";", null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Appareil appareil = cursorToAppareil(cursor);
        db.close();
        return appareil;
    }

    public Cursor fetchAllCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + NOM_TABLE_CATEGORIE + ";", null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        db.close();
        return cursor;
    }
}
