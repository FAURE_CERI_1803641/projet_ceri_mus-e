package com.example.projetcerimuse.Model;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

public class Appareil implements Parcelable {
    public String id;
    public String nom;
    public String categories;
    public String description;
    public String timeFrame;
    public int annee;
    public String marque;
    public String detailsTechnique;
    public boolean fonctionnel;
    public String lienIcone;
    public String lienImage;

    public Appareil(){
        this.nom = "default";
    }

    public Appareil(String id, String nom, String categories, String description, String timeFrame,
                    int annee, String marque, String detailsTechnique, boolean fonctionnel, String lienIcone){
        this.id = id;
        this.nom = nom;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.annee = annee;
        this.marque = marque;
        this.detailsTechnique = detailsTechnique;
        this.fonctionnel = fonctionnel;
        this.lienIcone = lienIcone;
    }

    protected Appareil(Parcel in) {
        id = in.readString();
        nom = in.readString();
        categories = in.readString();
        description = in.readString();
        timeFrame = in.readString();
        annee = in.readInt();
        marque = in.readString();
        detailsTechnique = in.readString();
        fonctionnel = in.readByte() != 0;
        lienIcone = in.readString();
        lienImage = in.readString();
    }

    public static final Creator<Appareil> CREATOR = new Creator<Appareil>() {
        @Override
        public Appareil createFromParcel(Parcel in) {
            return new Appareil(in);
        }

        @Override
        public Appareil[] newArray(int size) {
            return new Appareil[size];
        }
    };

    public void setId(String id){
        this.id = id;
        return;
    }

    public void setNom(String nom){
        this.nom = nom;
        return;
    }

    public void setCategories(String categories){
        this.categories = categories;
        return;
    }



    public void setTimeFrame(String timeFrame){
        this.timeFrame = timeFrame;
        return;
    }

    public void setAnnee(int annee){
        this.annee = annee;
        return;
    }

    public void setMarque(String marque){
        this.marque = marque;
        return;
    }

    public void setDetailsTechnique(String detailsTechnique){
        this.detailsTechnique = detailsTechnique;
        return;
    }

    public void setFonctionnel(boolean fonctionnel){
        this.fonctionnel = fonctionnel;
        return;
    }

    public void setLienIcone(String lienIcone){
        this.lienIcone = lienIcone;
        return;
    }

    public void setLienImage(String lienImage){
        this.lienImage = lienImage;
        return;
    }

    public String getId(){
        return this.id;
    }

    public String getNom(){
        return this.nom;
    }

    public String getCategories(){
        return this.categories;
    }

    public String getDescription(){
        return this.description;
    }

    public String getTimeFrame(){
        return this.timeFrame;
    }

    public int getAnnee(){
        return this.annee;
    }

    public String getMarque(){
        return this.marque;
    }

    public String getDetailsTechniques(){
        return this.detailsTechnique;
    }

    public boolean isFonctionnel(){
        return this.fonctionnel;
    }

    public String getLienIcone(){
        return this.lienIcone;
    }

    public String getLienImage(){
        return this.lienImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.nom);
        dest.writeString(this.categories);
        dest.writeString(this.description);
        dest.writeString(this.timeFrame);
        dest.writeInt(this.annee);
        dest.writeString(this.marque);
        dest.writeString(this.detailsTechnique);
        dest.writeBoolean(this.fonctionnel);
        dest.writeString(this.lienIcone);
    }
}
