package com.example.projetcerimuse.Model;

import java.util.Collection;
import java.util.HashMap;

public class AppareilList {

    private static HashMap<String, Appareil> hashMap = init();

    private static HashMap<String, Appareil> init() {
        HashMap<String, Appareil> res = new HashMap<>();
        /*
        res.put("tnx", new Appareil("tnx", "TO7/70", "ordinateur de bureau|ordinateur|8 bits", "Ordinateur personnel français, sélectionné aux côtés du Thomson MO5 pour l'équipement informatique des écoles, collèges et lycées dans le cadre du plan « Informatique pour tous » en 1985.", "1980", 1984, "Thompson", "CPU 8 bits (Motorola 6809E) à 1 MHz|RAM : 48 Kio (+16 Kio VRAM)|ROM : 6 Kio (avec BASIC intégré)|Crayon optique|Lecteur de cartouches|Raccordement possible d'un lecteur de cassettes ou d'un lecteur de disquettes (5¼ pouces)|Câble Péritel intégré", true, "IMG_1607", "IMG_1642"));
        res.put("8xi", new Appareil("8xi", "Lecteur de cassettes pour TO7", "périphérique|support de stockage", "Lecteur permettant d'utiliser des cassettes audio comme support de stockage de données. L'avantage des cassettes était un prix très inférieur à celui d'un lecteur de disquettes, la possibilité d'utiliser des cassettes..", "1980", 1984, "Thompson", "900 bits/s", true, "", ""));
        res.put("flh", new Appareil("flh", "Téléviseur N&B", "périphérique|écran", "Dans les années 1970 et 1980, les micro-ordinateurs personnels d'entrée et milieu de gamme (comme ici le ZX81, le C64, l'Alice et l'Apple //c) étaient souvent reliés à un téléviseur en lieu et place d'un moniteur dédié, principalement pour une raison budgétaire...", "1970", 0, "Pathé-Marconi", "Écran noir et blanc", true, "IMG_1581", "IMG_1579|IMG_1578"));
        res.put("rkj", new Appareil("rkj", "Souris pour station HP", "périphérique", "Souris pour station de travail HP, à connecter sur un port HP-HIL (type de bus créé par Hewlett-Packard pour les périphériques d'entrée, permettant de chaîner jusqu'à 7 appareils par bus).", "1980|1990", 0, "HP", "Mécanisme à boule|Connecteur SDL pour bus HP-HIL|3 boutons", false, "IMG_1649", "IMG_1650|IMG_1653|IMG_1654"));
        res.put("0qf", new Appareil("0qf", "Cartes PCMCIA", "périphérique|réseau", "Cartes fax/modem, Ethernet, wifi, pour extension d'ordinateurs portables disposant d'un port PCMCIA.", "1990|2000", 1990, "", "Fax/modem V.34 (28,8 kb/s)|Ethernet 10 Mb/s à utiliser avec un adaptateur pour RJ45 ou câble coaxial|Cartes de type II (épaisseur 5 mm)", false, "IMG_1724", "IMG_1725|IMG_1726"));
        res.put("hsv", new Appareil("hsv", "Lecteur de cartouches amovibles 88 Mio", "périphérique|support de stockage|SCSI", "Les cartouches SyQuest dans leurs versions 44 et 88 Mo constituaient la solution la plus répandue (en particulier dans le monde Macintosh) pour les échanges de données volumineuses. Elles étaient très utilisées dans les domaines de la publication assistée par ordinateur et du multimédia. Les cartouches contenaient les plateaux de disques durs, les têtes de lecture/écriture étant dans le lecteur...", "1990", 1991, "SyQuest Technology", "Connexion SCSI|Capacité de 88 Mo par cartouche|Cartouches à disque dur, format 5¼ pouces", false, "IMG_1683", "IMG_1684|IMG_1686"));
        res.put("lf8", new Appareil("lf8", "Bouchon pour Ethernet 10Base2", "réseau|câble/adaptateur", "Terminateur (communément appelé « bouchon ») à brancher à l'extrémité d'un réseau Ethernet 10Base2 (câble coaxial fin), contenant une résistance pour empêcher la réflexion du signal. Il est nécessaire de disposer des terminateurs aux deux extrémités du réseau pour que celui-ci puisse fonctionner...", "1980|1990", 1980, "", "Résistance de 50 Ω", false, "", ""));
        res.put("kyt", new Appareil("kyt", "Toughbook", "ordinateur|ordinateur portable", "PC portable « durci »", "2000", 2000, "Panasonic", "", false, "", ""));
        res.put("c1z", new Appareil("c1z", "Mémoire à tores de ferrite", "composant", "", "1950|1960|1970", 1950, "", "Capacité de 32 octets", false, "", ""));
        res.put("ry8", new Appareil("ry8", "Minitel 2", "réseau|terminal de communication", "Minitel avec clavier AZERTY.", "1980", 1980, "Telic Alcatel", "Écran 25 lignes × 40 colonnes à 8 niveaux de gris|Modem V23 (1200/75 bits/s, retournable)", true, "IMG_1590", "IMG_1591|IMG_1593|IMG_1589"));
        res.put("4w6", new Appareil("4w6", "ScanMan 32", "périphérique|SCSI", "Scanner à main pour Macintosh. Il permet de numériser une page complète en plusieurs passes (deux passes pour une feuille A4). Le logiciel accompagnant le scanner aide à combiner les images en une seule, à condition d'avoir suivi des trajectoires...", "1980|1990", 1989, "Logitech", "Production de fichiers aux formats Mac Paint, Pict et TIFF|Connexion à l'ordinateur en SCSI, via le boîtier d'interface|Numérisation en mode trait ou en 32 niveaux de gris|Résolution de 400 ppp", true, "", ""));
        res.put("cdn", new Appareil("cdn", "Calculatrice du programmeur", "ordinateur|ordinateur de poche", "Calculatrice dotées de fonctions dédiées au calcul en hexadécimal et en octal.", "1980", 1984, "Texas Instruments", "", false, "IMG_1648", "IMG_1679"));
        res.put("gex", new Appareil("gex", "Disquettes 3½ pouces double densité", "support de stockage", "Première génération du format de disquettes 3½ pouces initié par Sony. Ce format a été inauguré par le premier modèle de Macintosh, sorti en janvier 1984, avant d'être adopté en quelques années sur tous les autres ordinateurs.", "1980", 1984, "", "Double face|Capacité de 720 Kio (compatibles PC) ou 800 Kio (Mac)", false, "", ""));
        res.put("tsk", new Appareil("tsk", "Macintosh PowerBook 145", "ordinateur|SCSI|ordinateur portable", "Le PowerBook 145 était une évolution mineure (changement de processeur) de la gamme des PowerBook introduite en 1991. Cette gamme est notable pour avoir inauguré le design moderne des ordinateurs portables : clavier en arrière près de l'écran...", "1990", 1992, "Apple", "CPU 32 bits (Motorola 68030) à 25 MHz|RAM : 2 à 8 Mio|Écran 10 pouces monochrome (LCD à matrice passive), définition de 640 × 400|Disque dur interne SCSI de 40 Mo|Lecteur de disquettes 3,5 pouces 1440 Kio|Ports SCSI, série (×2), ADB, entrée/sortie audio|Poids : 3,1 kg", false, "", ""));
        res.put("4zh", new Appareil("4zh", "MTM", "ordinateur de poche|ordinateur|prototype", "Prototype de smartphone (à l'époque : « terminal multimédia mobile ») conçu dans le cadre d'un projet européen auquel a participé le LIA (en charge d'intégrer la reconnaissance de la parole et du locuteur dans le système). Le matériel se base sur le PDA...", "2001", 2001, "LIA", "CPU 32 bits à 206 MHz|RAM : 32 Mio|Emplacements prévus pour caméra et lecteur de cartes magnétiques|Connexion UMTS (3G)|Applications intégreés : télémédecine, guide touristique, apprentissage à distance", true, "PlaquetteMTM-Couverture", "PlaquetteMTM-Page1|PlaquetteMTM-Page2"));
*/
        return res;
    }

    public static String[] getNameArray() {
        return hashMap.keySet().toArray(new String[hashMap.size()]);
    }

    public static String[] getAppareilName() {
        String[] result = new String[hashMap.size()];
        Object[] collection = (Object[]) hashMap.values().toArray();
        for(int i = 0; i < hashMap.size(); i++){
            Appareil tmp = (Appareil)collection[i];
            result[i] = tmp.getNom();
        }
        return result;
    }

    public static Appareil getAppareil(String name) {
        return hashMap.get(name);
    }


}
